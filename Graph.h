#include<fstream>
#include<iostream>
#include<vector>
#include<set>
#include <sstream>
#include <string>

//виртуальный класс
class BaseGraphPresentation
{
public:

	int N; //вершины
	int M; //рёбра 
	bool D; //ориентированность
	bool W; //взвешенность
	char IND;
	virtual void readGraph(std::string fileName);
	virtual void addEdge(int from,int to, int weight);
	virtual void removeEdge(int from,int to);
	virtual int changeEdge(int from,int to,int newWeight); //возвращает старое значение веса ребра
	virtual void writeGraph(std::string fileName);

};

//матрица смежности 
class AdjMatrixGraph: public BaseGraphPresentation
{
private:
	friend class ListOfEdgesGraph;
	friend class AdjListGraph;

	std::vector< std::vector<int>> arr;

	void CreateArray(int n)
	{
		arr.resize(n);
		for(int i = 0; i < n; i++)
			arr[i].resize(n);
	}

	AdjMatrixGraph(AdjMatrixGraph& a)
	{
		N = a.N;
		M = a.M;
		IND = 'C';
		D = a.D;
		W = a.W;
		arr.resize(N);
		for(int i = 0; i < N; i++)
		{
			arr[i].resize(N);
			for(int j = 0; j < N; j++)
			{
				arr[i][j] = a.arr[i][j];
			}
		}
	}

	AdjMatrixGraph(ListOfEdgesGraph& a)
	{
		IND = 'C';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		CreateArray(N);
		for(int i = 0; i < M; i++)
		{
			if(W)
				arr[a.arr[i].first][a.arr[i].second] = a.arr[i].third;
			else
				arr[a.arr[i].first][a.arr[i].second] = 1;
			if(!D)
				arr[a.arr[i].second][a.arr[i].first] = arr[a.arr[i].first][a.arr[i].second];
		}
	}

	AdjMatrixGraph(AdjListGraph& a)
	{
		IND = 'C';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		CreateArray(N);
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < a.arr[i].size();i++)
			{
				if(W)
					arr[i][a.arr[i][j].first] = a.arr[i][j].second;
				else
					arr[i][a.arr[i][j].first] = 1;
				if(!D)
					arr[a.arr[i][j].first][i] = arr[i][a.arr[i][j].first];
			}
		}
	}

public:

	AdjMatrixGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'C';
	}

	AdjMatrixGraph(int n, int m, bool d, bool w)
	{
		N = n;
		M = m;
		D = d;
		W = w;
		CreateArray(n);
		IND = 'C';
	}

	void readGraph(std::string fileName)
	{
		char ch;
		std::ifstream fin(fileName, std::ios_base::in);

		M = 0;
		fin >> ch >> N >> D >> W;
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				fin >> arr[i][j];
				if(arr[i][j] != 0)
					M++;
			}
		}
		if(!D) M/=2;

		fin.close();
	}
	
	int changeEdge(int from,int to,int newWeight)
	{
		int oldWeight;
		arr[from][to] = newWeight;
		if(!D)
			arr[to][from] = newWeight;
		return oldWeight;
	}

	void addEdge(int from,int to, int weight)
	{
		M++;
		changeEdge(from, to, weight);
	}
	
	void removeEdge(int from,int to)
	{
		M--;
		changeEdge(from, to, 0);
	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc); 

		fout << IND << " " << N << std::endl
			<< D << " " << W << std::endl;
		for(int i = 0; i < N; i++)
		{
			fout << arr[i][0];
			for(int j = 1; j < N; j++)
				fout << " " << arr[i][j];
			fout << std::endl;
		}

		fout.close();
	}

};

//список смежности ( конструкторы)
class AdjListGraph: public BaseGraphPresentation
{//( i )---->( j )
private: 
	friend class AdjMatrixGraph;
	friend class ListOfEdgesGraph;
	std::vector<std::vector< std::pair<int,int> > > arr;

	void CreateArray(int n)
	{
		arr.resize(n);
		for(int i = 0; i < n; i++)
			arr[i].resize(0); 
	}

	int searchJ(int i, int first)
	{
		for(int j = 0; j < arr[i].size();j++)
		{
			if(arr[i][j].first == first)
			{
				return j;
			}
		}
		return -1;
	}

	AdjListGraph(AdjMatrixGraph& a)
	{
		
	}

	AdjListGraph(ListOfEdgesGraph& a)
	{
		
	}

	AdjListGraph(AdjListGraph& a)
	{
		IND = 'L';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		arr
	}

public:

	AdjListGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'L';
	}

	AdjListGraph(int n, int m, bool d, bool w)
	{
		N = n;
		M = m;
		D = d;
		W = w;
		IND = 'L';
		CreateArray(n);
	}

	void readGraph(std::string fileName)
	{
		char ch;
		int k;
		std::pair<int, int> p(0,0);
		std::ifstream fin(fileName, std::ios_base::in);
		std::string line;

		M = 0;
		fin >> ch >> N >> D >> W;
		for(int i = 0; i < N; i++)
		{
			//fin >> k;
			std::getline(fin, line);
			std::istringstream stream(line);
			while(stream >> p.first)
			{
				if(W)
					stream >> p.second;
				arr[i].push_back(p);
				M++;
			}
		}

		fin.close();
	}
	
	int changeEdge(int from,int to,int newWeight)
	{
		int oldWeight = -1;	

		int j = searchJ(to,from);
		if(j != -1)
		{
			oldWeight = arr[to][j].second;
			arr[to][j].second = newWeight;
		}
		else
		{
			j = searchJ(from,to);
			if(j != -1)
			{
				oldWeight = arr[from][j].second;
				arr[from][j].second = newWeight;
			}
		}

		return oldWeight;
	}

	void addEdge(int from,int to, int weight)
	{
		std::pair<int, int> a(to, weight);
		M++;
		arr[from].push_back(a);
	}
	
	void removeEdge(int from,int to)
	{
		int j = searchJ(to,from);
		arr[to].erase(arr[to].begin() + j);
		if(!D)
		{
			j = searchJ(from,to);
			arr[from].erase(arr[from].begin() + j);
		}
		M--;
		
	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc); 

		fout << IND << " " << N << std::endl
			<< D << " " << W << std::endl;
		for(int i = 0; i < N; i++)
		{
			fout << arr[i][0].first;
			if(W) 
				fout << " " << arr[i][0].second;

			for(int j = 1; j < arr[i].size(); j++)
			{
				fout << " " << arr[i][j].first;
				if(W)
					fout << " " << arr[i][j].second;
			}
			fout << std::endl;
		}

		fout.close();
	}

};

//список рёбер 
class ListOfEdgesGraph: public BaseGraphPresentation
{
private:

	friend class AdjMatrixGraph;
	friend class AdjListGraph;

	struct VectorInt3{
		int first;
		int second;
		int third;
	};

	std::vector< VectorInt3 > arr;

	int SearchEdge(int from, int to)
	{
		for(int i = 0; i < M; i++)
		{
			if((arr[i].first == from && arr[i].second == to) ||
				(!D && arr[i].first == to && arr[i].second == from))
			{
				return i;
			}
		}
		return -1;
	}

	ListOfEdgesGraph(AdjMatrixGraph& a)
	{
		IND = 'E';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		arr.resize(0);
		VectorInt3 p;
		p.third = 1;
		int k = 0;
		if(!D)
			k++;
		for(int i = 0; i < N; i++)
		{
			for(int j = i*k; j < N; j++)
			{
				if(a.arr[i][j] != 0)
				{
					p.first = i;
					p.second = j;
					if(W)
						p.third = a.arr[i][j];
					arr.push_back(p);
				}
			}
		}
		
	}

	ListOfEdgesGraph(ListOfEdgesGraph& a)
	{
		IND = 'E';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		arr.resize(M);
		for(int i = 0; i < M; i++)
		{
			arr[i] = a.arr[i];
		}
	}

	ListOfEdgesGraph(AdjListGraph& a)
	{
		IND = 'E';
		N = a.N;
		M = a.M;
		D = a.D;
		W = a.W;
		arr.resize(0);
		VectorInt3 p;
		p.third = 1;
		for(int i = 0; i < N; i++)
		{
			p.first = i;
			for(int j = 0; j < a.arr[i].size();j++)
			{
				p.second = a.arr[i][j].first;
				if(W)
					p.third = a.arr[i][j].second;
				arr.push_back(p);
			}
		}
	}

public:

	ListOfEdgesGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'E';
		arr.resize(0);
	}

	ListOfEdgesGraph(int n, int m, bool d, bool w)
	{
		N = n;
		M = m;
		D = d;
		W = w;
		IND = 'E';
		arr.resize(0);
	}

	void readGraph(std::string fileName)
	{
		char ch;
		VectorInt3 v;
		std::ifstream fin("fileName", std::ios_base::in);

		v.third = 1;
		M = 0;
		fin >> ch >> N >> M >> D >> W;
		for(int i = 0; i < M; i++)
		{
			fin >> v.first >> v.second;
			if(W)
				fin >> v.third;
		}

		fin.close();
	}
	
	int changeEdge(int from,int to,int newWeight)
	{
		int i = SearchEdge(from, to);
		int oldWeight = arr[i].third;
		arr[i].third = newWeight;
		return oldWeight;
	}

	void addEdge(int from,int to, int weight)
	{
		M++;
		VectorInt3 v;
		v.first = from;
		v.second = to;
		v.third = weight;
		arr.push_back(v);
	}
	
	void removeEdge(int from,int to)
	{
		M--;
		int i = SearchEdge(from,to);
		arr.erase(arr.begin() + i);
	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc); 

		std::fout << IND << " " << N << " " << M << endl
			<< D << " " << W << endl;
		for(int i = 0; i < N; i++)
		{
			fout << arr[i].first << " " << arr[i].second;
			if(W)
				fout << " " << arr[i].third;
			fout << endl;
		}

		fout.close();
	}

};

//главный класс
class Graph
{
private:

	BaseGraphPresentation* graph;
	AdjMatrixGraph* AMGraph;
	AdjListGraph* ALGraph;
	ListOfEdgesGraph* LEGraph;

	void CreateObjectGraph(char ind, int n = 1, int m = 1, bool d = false, bool w = false)
	{
		delete graph;//на всякий случай

		if(ind == 'C')
		{
			AMGraph = new AdjMatrixGraph();
			graph = AMGraph;
		}
		if(ind == 'L')
		{
			ALGraph = new AdjListGraph();
			graph = ALGraph;
		}
		if(ind == 'E')
		{
			LEGraph = new ListOfEdgesGraph();
			graph = LEGraph;
		}
	}

public:

	Graph()
	{
		CreateObjectGraph('C');
	}

	Graph(char ind)
	{
		CreateObjectGraph(ind);
	}

	Graph(char ind, int n, int m, bool d, bool w)
	{
		CreateObjectGraph(ind, n, m, d, w);
	}

	~Graph()
	{
		delete graph;
	}

	void readGraph(std::string fileName)
	{
		ifstream fin(fileName, ios_base::in);
		char ind;
		fin >> ind;
		fin.close();

		//delete graph;

		CreateObjectGraph(ind);

		graph->readGraph(fileName);
	}

	void addEdge(int from,int to, int weight)
	{
		graph->addEdge(from, to, weight);
	}

	void removeEdge(int from,int to)
	{
		graph->removeEdge(from, to);
	}

	int changeEdge(int from,int to,int newWeight)
	{
		graph->changeEdge(from, to, newWeight);
	}
	
	void transformToAdjMatrix()
	{
		if(graph->IND != 'C')
		{
			if(graph->IND == 'L')
				AMGraph = new AdjMatrixGraph(*ALGraph);
			if(graph->IND == 'E')
				AMGraph = new AdjMatrixGraph(*LEGraph);
			delete graph;
			graph = AMGraph;
		}
	}

	void transformToAdjList()
	{
		if(graph->IND != 'L')
		{
			if(graph->IND == 'C')
				ALGraph = new AdjListGraph(*AMGraph);
			if(graph->IND == 'E')
				ALGraph = new AdjListGraph(*LEGraph);
			delete graph;
			graph = ALGraph;
		}
	}

	void transformToListOfEdges()
	{
		if(graph->IND != 'E')
		{
			if(graph->IND == 'C')
				LEGraph = new ListOfEdgesGraph(*AMGraph);
			if(graph->IND == 'L')
				LEGraph = new ListOfEdgesGraph(*ALGraph);
			delete graph;
			graph = LEGraph;
		}
	}

	void writeGraph(std::string fileName)
	{
		graph->writeGraph(fileName);
	}

};